/*
	Filename: fir.h
		Header file
		FIR lab written for WES/CSE237C class at UCSD.

*/
#ifndef FIR_H_
#define FIR_H_

#include "ap_int.h"

#define NUM_BITS 16

const int N=128;

typedef ap_int<NUM_BITS>	coef_t;
typedef ap_int<NUM_BITS>	data_t;
typedef ap_int<NUM_BITS>	acc_t;

void fir (
  data_t *y,
  data_t x
  );

#endif
