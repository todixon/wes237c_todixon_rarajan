/*
	Filename: fir.h
		Header file
		FIR lab written for WES/CSE237C class at UCSD.

*/
#ifndef FIR_H_
#define FIR_H_

#include "ap_int.h"

#define NUMBITS 16
const int N=128;

typedef ap_int<NUMBITS>	coef_t;
typedef ap_int<NUMBITS>	data_t;
typedef ap_int<NUMBITS>	acc_t;

void fir (
  data_t *y,
  data_t x
  );

#endif
