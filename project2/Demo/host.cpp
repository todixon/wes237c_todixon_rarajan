#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#define NO_SAMPLE 512
#define NO_SAMPLE_X2 1024

int main(int argc, char *argv[]) {

  int fdr, fdw;
    
  int tologic[NO_SAMPLE_X2];
  int fromlogic[NO_SAMPLE_X2];
  FILE * fout;
  FILE * fin;

  int i;

  float * I;
  float * Q;
  float * R;
  float * Theta;

  float db_var;

	I=(float*)malloc(sizeof(float)*NO_SAMPLE);
	Q=(float*)malloc(sizeof(float)*NO_SAMPLE);
	R=(float*)malloc(sizeof(float)*NO_SAMPLE);
	Theta=(float*)malloc(sizeof(float)*NO_SAMPLE);
  
  fin=fopen("input_i.dat","r");
  for (i=0;i<NO_SAMPLE;i++)
  {
        fscanf(fin,"%f",&I[i]);
	
  }
  fclose(fin);

  fin=fopen("input_q.dat","r");
  for (i=0;i<NO_SAMPLE;i++)
  {
        fscanf(fin,"%f",&Q[i]);
	
  }
  fclose(fin);


  //Write Code Here: put I[] and Q[] into tologic[]. Hint: make sure this is compatible with how you receive input in the xillybus_wrapper function in HLS
 
int j=0;
int isamp;
for(isamp=0; isamp < NO_SAMPLE; isamp++) {
	tologic[2*isamp] = *((int *)&I[isamp]);
	tologic[2*isamp+1] = *((int *)&Q[isamp]);
}
	
  
  //Write Code Here: Xillybus APIs open(), write(), read() and close() just like lab 1.
	//1. open read and write channels using fdr, fdw
  
	fdr = open("/dev/xillybus_read_32", O_RDONLY);
	fdw = open("/dev/xillybus_write_32", O_WRONLY);
	
	if ((fdr < 0) || (fdw < 0)) {
		perror("Failed to open Xillybus device file(s)");
		exit(1);
	}
	
	//2. write data into and read data from the FPGA
	
	write(fdw, (void *) &tologic, sizeof(int)*NO_SAMPLE_X2);
	read(fdr, (void *) &fromlogic, sizeof(int)*NO_SAMPLE_X2);
  
	//3. when it is done reading, close the channels
	//read the tutorial for details

    close(fdr);
	close(fdw);
	
  //Write Code Here: decode the data in fromlogic[] into R[] and Theta[]. Hint: make sure this is compatible with how you send output in the xillybus_wrapper function in HLS
int k=0;
for(isamp=0; isamp < NO_SAMPLE; isamp++) {
	R[isamp] = *((float *) &fromlogic[2*isamp]);
	Theta[isamp] = *((float *) &fromlogic[2*isamp+1]);
}



  fout=fopen("output_R.txt","w");
  for (i=0;i<NO_SAMPLE;i++)
  {
	fprintf(fout,"%d ",i);
	fprintf(fout,"%f ",R[i]);
	fprintf(fout,"\n");
  }
  fclose(fout);

  fout=fopen("output_Theta.txt","w");
  for (i=0;i<NO_SAMPLE;i++)
  {
	fprintf(fout,"%d ",i);
	fprintf(fout,"%f ",Theta[i]);
	fprintf(fout,"\n");
  }
  fclose(fout);

  printf("Thetas at the R peaks are:\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n",Theta[31],Theta[63],Theta[95],Theta[127],Theta[159],Theta[191],Theta[223],Theta[255],Theta[287],Theta[319],Theta[351],Theta[383],Theta[415],Theta[447],Theta[479],Theta[511]);



  free(I);
  free(Q);
  free(R);
  free(Theta);


  return 0;
}
