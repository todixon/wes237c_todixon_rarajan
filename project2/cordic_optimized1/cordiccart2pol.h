#ifndef CORDICCART2POL_H
#define CORDICCART2POL_H

#include "ap_fixed.h"

#define NO_ITER 16

#define PI 3.1415926535897932846

#define NUMBITS 32
#define FRACTIONAL 16
typedef ap_fixed<NUMBITS, FRACTIONAL> data_t;
typedef ap_fixed<NUMBITS, FRACTIONAL> acc_t;

void cordiccart2pol(data_t x, data_t y, data_t * r,  data_t * theta);

#endif
