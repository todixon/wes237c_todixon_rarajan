/*
	Filename: fir.cpp
		Complex FIR or Match filter
		firI1 and firI2 share coef_t c[N]
		firQ1 and firQ2 share coef_t c[N]
		
	INPUT:
		I: signal for I sample
		I: signal for Q sample

	OUTPUT:
		X: filtered output
		Y: filtered output

*/

#include "phasedetector.h"

void firI1 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};

	static data_t shift_reg[N];
	acc_t acc = 0;;
	data_t data;
	int i;

	shift_accum:
	for(i=N-1; i>=0; i--) {
		if(i==0) {
			shift_reg[0] = x;
			data = x;
		} else {
			shift_reg[i] = shift_reg[i-1];
			data = shift_reg[i];
		}
		acc+=data*c[i];
	}
	*y = acc;
}
	
void firI2 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};

	// Write your code here
	static data_t shift_reg[N];
	acc_t acc = 0;;
	data_t data;
	int i;

	shift_accum:
	for(i=N-1; i>=0; i--) {
		if(i==0) {
			shift_reg[0] = x;
			data = x;
		} else {
			shift_reg[i] = shift_reg[i-1];
			data = shift_reg[i];
		}
		acc+=data*c[i];
	}
	*y = acc;
}

void firQ1 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};

	// Write your code here
	static data_t shift_reg[N];
	acc_t acc = 0;;
	data_t data;
	int i;

	shift_accum:
	for(i=N-1; i>=0; i--) {
		if(i==0) {
			shift_reg[0] = x;
			data = x;
		} else {
			shift_reg[i] = shift_reg[i-1];
			data = shift_reg[i];
		}
		acc+=data*c[i];
	}
	*y = acc;
}

void firQ2 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};

	// Write your code here

	static data_t shift_reg[N];
	acc_t acc = 0;;
	data_t data;
	int i;

	shift_accum:
	for(i=N-1; i>=0; i--) {
		if(i==0) {
			shift_reg[0] = x;
			data = x;
		} else {
			shift_reg[i] = shift_reg[i-1];
			data = shift_reg[i];
		}
		acc+=data*c[i];
	}
	*y = acc;
}

void fir (
  data_t I,
  data_t Q,

  data_t *X,
  data_t *Y
  ) {

	// Write your code here
	data_t II, QQ, IQ, QI;

	firI1(&II,I);
	firQ1(&QQ,Q);
	firI2(&QI,Q);
	firQ2(&IQ,I);

	// Calculate X
	*X = II + QQ;
	
	// Calculate Y
	*Y = QI - IQ;
}


