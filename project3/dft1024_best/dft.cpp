#include<math.h>
#include "dft.h"
#include"coefficients1024.h"

void dft(DTYPE real_sample[SIZE], DTYPE imag_sample[SIZE], DTYPE real_out[SIZE], DTYPE imag_out[SIZE])
{
	for(int k = 0; k < SIZE; k++)
	{
		real_out[k] = 0.0;
		imag_out[k] = 0.0;
	}

	for (int j = 0; j < SIZE; j++)
	{


		for(int i = 0; i<SIZE; i++)
		{
#pragma HLS UNROLL factor=32
				real_out[i] += real_sample[j]*cos_coefficients_table[(j*i)%SIZE] + imag_sample[j]*sin_coefficients_table[(j*i)%SIZE];
				imag_out[i] += real_sample[j]*sin_coefficients_table[(j*i)%SIZE] - imag_sample[j]*cos_coefficients_table[(j*i)%SIZE];
		}
	}

}
