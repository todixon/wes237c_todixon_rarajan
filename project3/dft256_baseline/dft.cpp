#include<math.h>
#include "dft.h"
#include"coefficients256.h"

void dft(DTYPE real_sample[SIZE], DTYPE imag_sample[SIZE])
{
  DTYPE temp_r[SIZE];
  DTYPE temp_i[SIZE];
  
  DTYPE w;
  DTYPE c,s;

  for(int ii = 0; ii < SIZE; ii++)
  {
	  temp_r[ii] = 0.0;
	  temp_i[ii] = 0.0;

	w = -2.0*3.141592653589*(DTYPE)ii/(DTYPE)SIZE;
    for(int jj = 0; jj < SIZE; jj++)
    {
      s = sin(jj*w);
      c = cos(jj*w);
      
      temp_r[ii] += ((real_sample[jj]*c) - (imag_sample[jj]*s));
      temp_i[ii] += ((imag_sample[jj]*c) + (real_sample[jj]*s));
    }
  }
  
  for(int ii = 0; ii < SIZE; ii++)
  {
	  real_sample[ii] = temp_r[ii];
	  imag_sample[ii] = temp_i[ii];
  }
  
  
	
}

