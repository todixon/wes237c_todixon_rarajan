#include<math.h>
#include "dft.h"
#include"coefficients256.h"

void split(DTYPE in[SIZE], DTYPE out1[SIZE], DTYPE out2[SIZE]) {
	L1:
	for(int i=0; i<SIZE; i++) {
		out1[i] = in[i];
		out2[i] = in[i];
	}
}

void splitconst(const DTYPE in[SIZE], DTYPE out1[SIZE], DTYPE out2[SIZE]) {
	L1:
	for(int i=0; i<SIZE; i++) {
		out1[i] = in[i];
		out2[i] = in[i];
	}
}

void dft(DTYPE real_sample[SIZE], DTYPE imag_sample[SIZE], DTYPE real_out[SIZE], DTYPE imag_out[SIZE])
{
#pragma HLS DATAFLOW

	  DTYPE w;
	  DTYPE c,s;

	  DTYPE real_sample1[SIZE], real_sample2[SIZE];
	  DTYPE imag_sample1[SIZE], imag_sample2[SIZE];
	  DTYPE sincoef1[SIZE], sincoef2[SIZE];
	  DTYPE coscoef1[SIZE], coscoef2[SIZE];

	  split(real_sample,real_sample1,real_sample2);
	  split(imag_sample,imag_sample1,imag_sample2);
	  splitconst(sin_coefficients_table,sincoef1,sincoef2);
	  splitconst(cos_coefficients_table,coscoef1,coscoef2);

	for (int j = 0; j < SIZE; j++)
	{
		real_out[j] = 0.0;
		for(int i = 0; i<SIZE; i++)
		{
				real_out[j] += real_sample1[i]*coscoef1[(j*i)%SIZE] + imag_sample1[i]*sincoef1[(j*i)%SIZE];
		}
	}

	for (int j = 0; j < SIZE; j++)
	{
		imag_out[j] = 0.0;
		for(int i = 0; i<SIZE; i++)
		{
				imag_out[j] += real_sample2[i]*sincoef2[(j*i)%SIZE] - imag_sample2[i]*coscoef2[(j*i)%SIZE];
		}
	}
}