#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#define NO_SAMPLE 1024
#define NO_SAMPLE_X2 1024

#define SIZE 1024

typedef float DTYPE;

int main(int argc, char *argv[]) {
  int fdr, fdw;

  const int FSIZE=2000;
  DTYPE In_R[FSIZE], In_I[FSIZE];
  FILE *fp;
  FILE *fout;

  fp = fopen("input.dat", "r");

  printf("Reading INPUTS\n");
  for (int i = 0; i < FSIZE; i++) {
	fscanf(fp, "%f %f\n", &In_R[i], &In_I[i]);
  }

  fclose(fp);


  fdr = open("/dev/xillybus_read_32", O_RDONLY);
  fdw = open("/dev/xillybus_write_32", O_WRONLY);

  int D[FSIZE] = {0};
  int dindex = 0;
  int iindex = 0;
  DTYPE In_R_block[SIZE], In_I_block[SIZE];
  DTYPE Out_R_block[SIZE], Out_I_block[SIZE];
  DTYPE Out_R[FSIZE], Out_I[FSIZE];
  
  printf("Running Loops\n");
  int j = 0;
  for (int i = 0; i < FSIZE; i++) {
	int tohw[2];
	uint32_t from_hw[1];

	In_R_block[j] = In_R[i];
	In_I_block[j] = In_I[i];
	tohw[0] = *((int*)&In_R_block[j]);
	tohw[1] = *((int*)&In_I_block[j]);
	int bites = 0;
	while(bites < sizeof(tohw))
	{
		bites += write(fdw, ((char*)tohw) + bites, sizeof(tohw)-bites);
	}

	printf("wrote bytes! %d\n",i);

	bites = 0;
	while(bites<sizeof(from_hw))
	{
		bites += read(fdr,((char*)from_hw) + bites, sizeof(from_hw)-bites);
  	}

	printf("read bytes! %d\n",i);

	D[dindex] = from_hw[0];
	dindex++;

  }

close(fdw);
  close(fdr);

  printf("Comparing with golden output\n");
	fp = fopen("out.gold.dat", "r");

	int gold, result = 0;
	for( int i=0 ; i < FSIZE ; i++ ){
		fscanf(fp, "%d", &gold );
		if (gold != D[i]){
			printf("miss match: i: %d\tgolden: %d\toutput: %d\n", i, gold, D[i] );
			result = 1;

		}
	}

	fclose(fp);

  fout=fopen("output.txt","w");
  for (int i=0;i<FSIZE;i++)
  {
	fprintf(fout,"%d ",D[i]);
	fprintf(fout,"\n");
  }
  fclose(fout);
  
  return 0;
}
