#include "fft.h"

static unsigned short count;
static DTYPE xr[SIZE];
static DTYPE xi[SIZE];
static DTYPE xr_out[SIZE];
static DTYPE xi_out[SIZE];
static int   dout[SIZE];

void ofdm_receiver(volatile uint32_t *inptr, volatile uint32_t *outptr)
{
#pragma AP interface ap_fifo port=inptr
#pragma AP interface ap_fifo port=outptr
#pragma AP interface ap_ctrl_none port=return

	*outptr++ = dout[count];

	xr[count] = (DTYPE)*inptr++;
	xi[count] = (DTYPE)*inptr++;
	count++;
	if(count == SIZE){
		count = 0;
		demod(xr, xi, dout, xr_out, xi_out);
	}
}

void xillybus_wrapper(int *in, int *out) {
#pragma AP interface ap_fifo port=in
#pragma AP interface ap_fifo port=out
#pragma AP interface ap_ctrl_none port=return
	DTYPE xr[2];
	int xr_temp = 0;
	int xi_temp = 0;
	int xrout_temp = 0;
	int xiout_temp = 0;

	uint32_t xr_out, xi_out;
	xr_temp = *in++;
	xr[0] = *((float *)&xr_temp);
	//ofdm_receiver(xr,xr_out);

	xi_temp = *in++;
	xr[1] = *((float *)&xi_temp);
	ofdm_receiver((uint32_t *)xr,&xi_out);
	*out++=xi_out;
}
