
#include <stdio.h>
#include <stdlib.h>
//#include <fcntl.h>
#include "fft.h"

DTYPE In_R[SIZE], In_I[SIZE];
DTYPE Out_R[SIZE], Out_I[SIZE];

//void demod(DTYPE X_R[SIZE], DTYPE X_I[SIZE], int D[SIZE], DTYPE OUT_R[SIZE], DTYPE OUT_I[SIZE]);

int main()
{
	const int FSIZE=35000;
	DTYPE In_R[FSIZE], In_I[FSIZE];
	FILE *fp;

	fp = fopen("input.dat", "r");

	printf("Reading INPUTS\n");
	for (int i = 0; i < FSIZE; i++) {
		fscanf(fp, "%f %f\n", &In_R[i], &In_I[i]);
	}

	fclose(fp);


	//Perform FFT
	int D[FSIZE] = {0};
	int dindex = 0;
	int iindex = 0;
	dindex = 1024; //hack the golden output

	for (int i = 0; i < FSIZE; i+=SIZE) {
		uint32_t tohw[2*SIZE];
		uint32_t from_hw[SIZE];

		for (int j = 0; j<SIZE; j++) {
			tohw[2*j  ] = *((uint32_t *)&In_R[i+j]);
			tohw[2*j+1] = *((uint32_t *)&In_I[i+j]);
		}
		ofdm_receiver(&tohw[0], &from_hw[0]);
		for (int j = 0; j<SIZE; j++) {
			D[dindex] = from_hw[j];
			dindex++;
		}
	}

	printf("Comparing with golden output\n");
	fp = fopen("out.gold.dat", "r");

	int gold, result = 0;
	for( int i=0 ; i < FSIZE ; i++ ){
		fscanf(fp, "%d", &gold );
		if (gold != D[i]){
			printf("miss match: i: %d\tgolden: %d\toutput: %d\n", i, gold, D[i] );
			result = 1;

		}
	}

	fclose(fp);

	//Check against golden output.
	printf ("Comparing against output data \n");

	if (result) {
		fprintf(stdout, "*******************************************\n");
		fprintf(stdout, "FAIL: Output DOES NOT match the golden output\n");
		fprintf(stdout, "*******************************************\n");
		return result;
	} else {
		fprintf(stdout, "*******************************************\n");
		fprintf(stdout, "PASS: The output matches the golden output!\n");
		fprintf(stdout, "*******************************************\n");
		return result;
	}

}
