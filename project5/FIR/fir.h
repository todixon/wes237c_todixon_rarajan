#ifndef FIR_H
#define FIR_H

#define MYCOUNT  128
#define W_ACC 30
#define I_ACC 6

#define W_TAPS 32
#define I_TAPS 1

void fir(float * result, float input,  unsigned short do_filter);

#endif
