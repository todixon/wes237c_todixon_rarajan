/* VOLK */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "volk.h"

void volk(float outputVector[MYCOUNT], float inputVector[MYCOUNT])
{
#pragma HLS ARRAY_PARTITION variable=inputVector block factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=outputVector cyclic factor=32 dim=1
	//write your code here
	// https://github.com/gnuradio/volk/blob/master/kernels/volk/volk_32fc_x2_multiply_conjugate_32fc.h
	//	static inline void
	//	volk_32fc_x2_multiply_conjugate_32fc_generic(lv_32fc_t* cVector, const lv_32fc_t* aVector,
	//	                                             const lv_32fc_t* bVector, unsigned int num_points)
	//	{
	//	  lv_32fc_t* cPtr = cVector;
	//	  const lv_32fc_t* aPtr = aVector;
	//	  const lv_32fc_t* bPtr=  bVector;
	//	  unsigned int number = 0;
	//
	//	  for(number = 0; number < num_points; number++){
	//	    *cPtr++ = (*aPtr++) * lv_conj(*bPtr++);
	//	  }
	//	}

float ar, br, ai, bi;

	kernel:
	for(int i = 0; i < MYCOUNT-2; i+=2)
	{
#pragma HLS PIPELINE II=2
		ar = inputVector[i+2];
		ai = inputVector[i+3];
		br = inputVector[i];
		bi = inputVector[i+2];

		outputVector[i] = (ar*br) + (ai*bi);
		outputVector[i+1] = (ai*br) - (ar*bi);

//		outputVector[i] = (inputVector[i+2]*inputVector[i]) + (inputVector[i+3]*inputVector[i+1]);
//		outputVector[i+1] = (inputVector[i+3]*inputVector[i]) - (inputVector[i+2]*inputVector[i+2]);
	}
	

}
