#ifndef WBFM_H
#define WBFM_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MYCOUNT  8
#define W_ACC 60
#define I_ACC 6

#define W_TAPS 32
#define I_TAPS 1

void volk(float outputVector[MYCOUNT], float inputVector[MYCOUNT]);
void fir(float * result, float input, unsigned short do_filter);
void iir(float input, float *output, int size);
float fast_atan(float y, float x);

#endif
